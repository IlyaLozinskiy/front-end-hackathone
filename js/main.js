
(function ($) {
	"use strict";
	const helperStyle = "z-index: 10000; position: relative; background: white;"

	$('.column100').on('mouseover', function () {
		var table1 = $(this).parent().parent().parent();
		var table2 = $(this).parent().parent();
		var verTable = $(table1).data('vertable') + "";
		var column = $(this).data('column') + "";

		$(table2).find("." + column).addClass('hov-column-' + verTable);
		$(table1).find(".row100.head ." + column).addClass('hov-column-head-' + verTable);
	});

	$('.column100').on('mouseout', function () {
		var table1 = $(this).parent().parent().parent();
		var table2 = $(this).parent().parent();
		var verTable = $(table1).data('vertable') + "";
		var column = $(this).data('column') + "";

		$(table2).find("." + column).removeClass('hov-column-' + verTable);
		$(table1).find(".row100.head ." + column).removeClass('hov-column-head-' + verTable);
	});

	var reference = $('.link-btn');
	var popover = $('.popover');
	popover.hide();
	var currentPop;
	$(reference).on('click', (e) => {
		currentPop = popover.filter((i, el) => el.id.includes(e.target.id.split("-")[0]))
		var popper = new Popper(e.target, currentPop, {
			placement: 'top',
		});
		currentPop.show();
	})

	// 	$(document).on('click touchend', function(e) {
	// 	  var target = $(e.target);
	// 	  // ne need to reshow and recreate poper when click over popup so return;
	// 	  if(target.is(popover)) return;
	// 	  if (references.includes(target[0].id)) {
	// 		e.preventDefault();
	// 		pop = popover.filter((i, p)=> p.id.includes(target[0].id.split('-')[0]))
	// 		pop.show();

	// 		var popper = new Popper(target, pop, {
	// 		  placement: 'top',
	// 		});
	// 	  }else {
	// 		pop.hide();
	// 	  }
	//   });

})(jQuery);